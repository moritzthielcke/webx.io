/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package io.wx.core3.config;

/**
 *
 * @author Moritz
 */
public abstract class AbstractConfigurator implements Configurator{
    AppConfig configuration; 
    
    @Override
    public Configurator setAppConfig(AppConfig cfg) {
        this.configuration = cfg;
        return this;
    }

    @Override
    public AppConfig getAppConfig() {
       return this.configuration;
    }
        
    
}
