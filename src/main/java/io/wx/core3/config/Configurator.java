/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package io.wx.core3.config;

import java.util.Map;

/**
 * webx core configurator interface ( e.g. for module configuration and other core mechanics ) 
 *
 * @author moritz
 */
public interface Configurator {
    public Configurator setAppConfig(AppConfig cfg);
    public AppConfig getAppConfig();
    public void doConfig(Map modules);    
}
