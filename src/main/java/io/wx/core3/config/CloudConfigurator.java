/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package io.wx.core3.config;

import io.vertx.core.json.JsonObject;
import io.wx.core3.common.WXTools;
import io.wx.core3.di.l4j.Log4jModule;
import io.wx.core3.config.AppConfig.Stage;
import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Map;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.core.Appender;


/**
 * 
 * Basic configuration for cloudcontrol / openstack
 * accepts an AppConfig.java , but overrides certain configuration values based on System.getenv
 * 
 * @todo does inheritage makes sense here?
 * 
 *
 * @author moritz
 */
public class CloudConfigurator extends AbstractConfigurator{
    private static final Logger logger = LogManager.getLogger(CloudConfigurator.class);

    

    
    @Override
    public void doConfig(Map basics) {
        //set stage/host/port
        if(System.getenv("HOSTNAME")!=null){
            configuration.setHost(System.getenv("HOSTNAME"));
        }
        if(System.getenv("PORT")!=null){
            configuration.setPort(Integer.parseInt(System.getenv("PORT")));
        }
        configuration.setStage(getStage());
        
        //load cred file:
        String credfileName = System.getenv().get("CRED_FILE");
        if(credfileName==null){
             credfileName = "./cfg/addon.creds";
        }
        JsonObject loadCreds = WXTools.loadCredentials(credfileName);
        if(loadCreds != null){
            configuration.setAddonCredentials(loadCreds);                    
        }

        /*disable "out of the box" ssl in the cloud
        if(configuration.getStage() != Stage.LOCAL &&
            configuration.getStage() != Stage.TEST){
            logger.info("> disabling out-of-the-box ssl");
            configuration.setSSL(false);
        }*/
        
        //override log4j
        Log4jModule loggingModule = new Log4jModule();
        if(configuration.getStage() != Stage.LOCAL && configuration.getStage() != Stage.TEST){   
            Appender getAppender;
            switch(configuration.getStage()){
                case DEV: getAppender = loggingModule.getAppender("dev"); break;
                case QA:  getAppender = loggingModule.getAppender("qa");  break;
                default:  getAppender = loggingModule.getAppender("default"); break; 
            }
            if(getAppender != null){
                logger.info("> wiring logging to appender "+getAppender.getName());
                loggingModule.wireRootLogger(getAppender, Level.INFO, true);
            }  
            else{
                logger.warn("cant find root logger for Stage: "+configuration.getStage());
            }
        }
        basics.put("log4j", loggingModule);
    }
    
    


    /***
     * reads the deployment stage
     * @return 
     */
    public static Stage getStage() {
        if (System.getenv().get("DEP_NAME") == null) {
            return Stage.LOCAL;
        } 
        else {
            String[] getDeplomentName = System.getenv().get("DEP_NAME").split("\\/");
            String depName;
            if (getDeplomentName.length > 1) {
                depName = getDeplomentName[1];
            } else {
                depName = getDeplomentName[0];
            }
            switch (depName) {
                case "dev":
                    return Stage.DEV;
                case "default":
                    return Stage.DEFAULT;
                case "qa":
                    return Stage.QA;
                default:
                    return Stage.OTHER;
            }
        }
    }    

    
    /**
     * gets the directory for temp files
     * @return 
     */
    public static String getTmpDir(){
        if (System.getenv().get("TMPDIR") != null) {
            return System.getenv().get("TMPDIR");
        }
        return "./";
    }
    
    
}
