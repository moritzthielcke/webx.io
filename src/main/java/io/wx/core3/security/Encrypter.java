/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package io.wx.core3.security;

import java.security.SignatureException;

/**
 *
 * java.security encrypter interface
 * 
 * @author moritz
 */
public interface Encrypter {

    String decrypt(String encryptedData)  throws Exception;

    String encrypt(String Data) throws Exception;
    
    String sign(String data)  throws SignatureException;
    
}
