/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package io.wx.core3.common;

import java.util.Map;
import java.util.function.Consumer;
import org.apache.commons.beanutils.BeanUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 *
 * uses Beanutils to reflect a map to a bean
 * 
 * @author moritz
 */
public class Map2BeanConsumer implements Consumer<Map.Entry<String, String>>{
    private static final Logger logger = LogManager.getLogger(Map2BeanConsumer.class);    
    private boolean debug=false;
    private Object o;
    
    public Map2BeanConsumer(Object o){
        this.o = o;
    }

    public Object getObject() {
        return o;
    } 

    /**
     * is logging enabled?
     * 
     * @return 
     */
    public boolean isDebug() {
        return debug;
    }

    /***
     * sets the debug mode (log errors if a propertie can not be set)
     * @param debug 
     */
    public void setDebug(boolean debug) {
        this.debug = debug;
    }

       

    @Override
    public void accept(Map.Entry<String, String> t) {
        try {
            BeanUtils.setProperty( this.o, t.getKey(), t.getValue() );
        }
        catch (Exception ex) {
            if(debug){
                this.logger.error("cant set propertie "+t.getKey(),ex);
            }
        }
    }
    
}
