/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package io.wx.core3.common.controller;

import io.vertx.core.json.JsonObject;
import io.wx.core3.common.ApplicationDetails;
import io.wx.core3.http.RequestMapping;
import io.wx.core3.http.Resource;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

/**
 *
 * @author moritz
 */
@Resource(path="/about")
public class ApplicationDetailController {
    private static final ApplicationDetails appDetails = new ApplicationDetails();
    public static Set<String> showDetails = new HashSet<>(Arrays.asList("product",
                                                                        "version",
                                                                        "mavanagaiata.commit.id"));
    
    @RequestMapping
    public JsonObject getApplicationInfo(){
        return appDetails.getProperties(showDetails);
    }
    
    @RequestMapping(path = "/ram")
    public JsonObject getRAM(){
        return appDetails.getRAM();
    }    
}
