/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package io.wx.core3.di.l4j;

import com.google.inject.AbstractModule;
import com.google.inject.matcher.Matchers;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.core.Appender;
import org.apache.logging.log4j.core.Filter;
import org.apache.logging.log4j.core.LoggerContext;
import org.apache.logging.log4j.core.config.LoggerConfig;

/**
 *
 * @author moritz
 */
public class Log4jModule extends AbstractModule{
    private final LoggerContext ctx;   
    
    public Log4jModule(){
        ctx = (LoggerContext)LogManager.getContext(false);
    }
    
    @Override
    protected void configure() {
           bindListener(Matchers.any(), new Log4jTypeListener(ctx));
    }
        
     
    public Appender getAppender(String name){
        return ctx.getConfiguration().getAppender(name);
    }
    

    /**
     * 
     * @param a
     * @param loggingLevel
     * @param override
     * @return false if the given loggername was not found 
     */    
    public boolean wireRootLogger(Appender a, Level loggingLevel, boolean override){
        return wireLogger(LogManager.ROOT_LOGGER_NAME, a, loggingLevel, override);
    }


    /***
    * 
    * wireLogger(loggerName, a , loggingLevel, false)
    * 
    * @param loggerName
    * @param a
    * @param loggingLevel
    * @return false if the given loggername was not found 
    */
    public boolean wireLogger(String loggerName, Appender a, Level loggingLevel){
        return wireLogger(loggerName, a, loggingLevel, false);
    }    
    
    /***
     * 
     * @param loggerName
     * @param a
     * @param loggingLevel
     * @param override
     * @return  false if the given loggername was not found
     */
    public boolean wireLogger(String loggerName, Appender a , Level loggingLevel, boolean override){
        LoggerConfig lc = ctx.getConfiguration().getLoggerConfig(loggerName);
        clearAppenders(lc);
        wireLogger(lc, a, loggingLevel, null);
        /*for(LoggerConfig lc : ctx.getConfiguration().getLoggers().values()){
             if(lc.getName() != null && lc.getName().equals(loggerName)){
                 if(override){
                     clearAppenders(lc);
                 }
                 wireLogger(lc, a, loggingLevel, null);
                 return true;
             }
         }*/
        return false;
    }
    
    public Log4jModule clearAppenders(LoggerConfig lc){
        for(String appenderName : lc.getAppenders().keySet()){
            lc.removeAppender(appenderName);
        }
        return this;
    }
    
    public Log4jModule wireLogger(LoggerConfig lc, Appender a, Level loggingLevel, Filter f){
        lc.addAppender(a, loggingLevel, f);
        ctx.updateLoggers();
        return this;
    }
    
 
    
  
  
}
