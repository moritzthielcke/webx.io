/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package io.wx.core3.di;

import com.google.inject.AbstractModule;
import io.wx.core3.http.RequestController;


/**
 * Di Module for the RequestController
 * 
 * @author moritz
 */
public class RequestControllerModule extends AbstractModule{
    private RequestController ctr;
    
    public RequestControllerModule(RequestController ctr){
        this.ctr = ctr;
    }
    
    @Override
    protected void configure() {
        bind(RequestController.class).toInstance(ctr); //normal use
    }
    
}
