/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package io.wx.core3.http.traits;

import io.vertx.core.Handler;
import io.vertx.ext.web.RoutingContext;
import io.wx.core3.http.app.Application;

/**
 *
 * @author Jose Luis Conde Linares-  jlinares@aptly.de
 */
public class TraitFactory {

  public static Handler<RoutingContext> getAfterHandlerTrait(Class<? extends Trait> clazz, Application a) throws Exception {
    Trait trait = clazz.newInstance();
    return trait.setupAfterTrait(a);
  }
  public static Handler<RoutingContext> getBeforeHandlerTrait(Class<? extends Trait> clazz, Application a) throws Exception {
    Trait trait = clazz.newInstance();
    return trait.setupBeforeTrait(a);
  }
}
