/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package io.wx.core3.http;

import com.google.inject.Module;
import io.vertx.core.buffer.Buffer;
import io.vertx.core.http.HttpServerRequest;
import io.vertx.core.http.HttpServerResponse;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.FileUpload;
import io.vertx.ext.web.Route;
import io.vertx.ext.web.RoutingContext;
import io.vertx.ext.web.Session;
import io.vertx.ext.web.templ.TemplateEngine;
import io.wx.core3.http.cookies.CookieStore;
import io.wx.core3.http.exceptions.HandleResponseException;
import io.wx.core3.common.WXTools;
import io.wx.core3.http.session.CookieSessionEncrypter;
import io.wx.core3.templates.Template;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;


/**
 * Http RequestController implementation
 * 
 * @author moritz
 */
public class RequestControllerImpl implements RequestController {
    private static Logger logger = LogManager.getLogger(RequestControllerImpl.class);
    private final RoutingContext ctx;
    private final Core c;
    
    public RequestControllerImpl(RoutingContext ctx,  Core c){
        this.ctx = ctx;
        this.c = c;
    }
    
    /**
     * @return the current route this context is being routed through. 
     */
    @Override
    public Route getCurrentRoute(){
         return ctx.currentRoute();
    }
     
    /***
     * @return all the context data as a map
     */
    @Override
    public Map<String,Object> getData(){
        return ctx.data();
    }
    
    /**
     * Fail the context with the specified status code.
    *  This will cause the router to route the context to any matching failure handlers for the request. If no failure handlers match a default failure response will be sent.
     * @param statusCode 
     */
    @Override
    public void fail(int statusCode){
        ctx.fail(statusCode);
    }
    
    /**
     * Fail the context with the specified throwable.
    *  This will cause the router to route the context to any matching failure handlers for the request. If no failure handlers match a default failure response with status code 500 will be sent.
     * @param throwable 
     */
    @Override
    public void fail(Throwable throwable){
        ctx.fail(throwable);
    }
    
    /**
     * a set of fileuploads (if any) for the request
     * 
     * @return 
     */
    @Override
    public Set<FileUpload> getFileUploads(){
        return ctx.fileUploads();
    }
    
    /**
     * If the route specifies produces matches, e.g. produces `text/html` and `text/plain`, 
     * and the `accept` header matches one or more of these then this returns the most acceptable match.
     * @return 
     */
    @Override
    public String getAcceptableContentType(){
        return ctx.getAcceptableContentType();
    }
    
    /**
     * returns the responsebody for this request, this enables you to write the response when its ready
     * @return 
     */
    @Override
    public StringBuilder getResponseBody(){
        if(getData().get("_responsebody") == null){
            getData().put("_responsebody", new StringBuilder());
        }
        return (StringBuilder) getData().get("_responsebody");
    }
    
    
    /***
     * sets the response body
     * @param body
     * @return a reference to this, so the API can be used fluently
     */
    @Override
    public RequestController setResponseBody(StringBuilder body){
         getData().put("_responsebody", body);
         return this;
    }
    
   
    
    
    /*
    * Get the entire HTTP request body as a JsonObject.
    */
    @Override
    public JsonObject getBodyAsJson(){
         return ctx.getBodyAsJson();
    }
    
    /**
     * Get the entire HTTP request body as a Buffer.
     * 
     * @return 
     */
    @Override
    public Buffer getRequestBody(){
        return ctx.getBody();
    }
    
    /***
     * gets the entire HTTP request body as a string, assuming UTF-8 encoding. 
     * 
     * @return the body
     */
    @Override
    public String getRequestBodyAsString(){
      return ctx.getBodyAsString();
    }
    
    /***
     * Get the entire HTTP request body as a string, assuming the specified encoding.
     * 
     * @param encoding the encoding, e.g. "UTF-16"
     * @return  the body
     */
    @Override
    public String getRequestBodyAsString(String encoding){
        return ctx.getBodyAsString(encoding);
    }
    
    /**
     * Tell the router to route this context to the next matching route (if any). 
     * This method, if called, does not need to be called during the execution of the handler, 
     * it can be called some arbitrary time later, if required.
     */
    @Override
    public void next(){
        ctx.next();
    }
    
    @Override
    public String getNormalisedPath(){
        return ctx.normalisedPath();
    }
    
    /**
     * the HTTP request object
     * @return 
     */
    @Override
    public HttpServerRequest getRequest(){
        return ctx.request();
    }
    
    /**
     * the HTTP response object
     * 
     * @return 
     */
    @Override
    public HttpServerResponse getResponse(){
        return ctx.response();
    }
    
    @Override
    public Session getSession(){
       return ctx.session();
    }
    
    /***
     * saves the current session (cookie store only)
     * @return 
     */
    public RequestController saveSession(){
        //save session-object and cookies:
        CookieStore cookieStore = this.getCookieStore();
        //if we have a session encryper -> use it to store the session in the cookie:
        if(getData().get("_sessionEncrypter") != null){
            Session session = ctx.session();
            //@todo test , session doenst get saved if no ID
            if(session != null && session.id() != null){
                ((CookieSessionEncrypter)ctx.data().get("_sessionEncrypter")).saveSession(session, cookieStore);
            }
        }
        cookieStore.toHeader();    
        return this;
    }

    @Override
    public CookieStore getCookieStore(){ //@todo rewrite cookie store to use the internal cookie object
        if( getData().get("_cookiestore") == null ){            
            getData().put("_cookiestore", new CookieStore(ctx.request()));
        }             
        return (CookieStore) getData().get("_cookiestore");
    }
        
        
    /***
     * returns the underlying request context, ONLY use this if you sure about what you doing
     * normaly all imporant functions can be access thru the HttpController itself
     * @return 
     */
    @Override
    public RoutingContext getRoutingContext(){
        return ctx;
    }

    /**
     * is the request in debug mode ? 
     */
    @Override
    public Boolean isDebug(){
        if(getData().get("_isdebug") == null){
            setDebug(false);
        }
        return (Boolean)getData().get("_isdebug"); //gets set from app
    }
    
    /**
     * sets the debug mode for this request
     * @param b
     * @return 
     */
    @Override
    public RequestControllerImpl setDebug(Boolean b){
        getData().put("_isdebug", b);
        return this;
    }
    
    
   /***
    * returns the controller machtes for the given request (does not contain trait matches!)
    * if no match exists webx returns a 404
    * @return 
    */
   public int getMatches(){
       return  ctx.get("_routematches") != null ? (int)ctx.get("_routematches") : 0;
   }
 
   
   /**
    * adds a match to the given request 
    * @return 
    */
   public int doMatch(){
        int matches = getMatches()+1;
        ctx.data().put("_routematches", matches);      
        return matches;
   }    
   
 
    /***
     * gets the dependencie injection modules for the request scope
     * @return 
     */   
    @Override
    public Set<Module> getModules(){
         if( getData().get("_requestmodules") == null ){   
             getData().put("_requestmodules", new HashSet<>());       
         }
         return (HashSet<Module>)getData().get("_requestmodules");
    }
   
    /***
     * adds a dependencie injection module for the request scope
     * @param m
     * @return 
     */   
    @Override
    public RequestController addModule(Module m){
        getModules().add(m);
        return this;
    }


    /***
     *  handle response based on @param result. supports String.class, Template.class or JSON/XML Serialization
     * @param result     
     * @return  current instance     
     * @throws io.wx.core3.http.exceptions.HandleResponseException     
     */
    @Override
    public RequestControllerImpl doResponse(Object result) throws HandleResponseException {
        /* render result */       
        try{
            /* specific content type defined? */
            String contentType = this.getResponse().headers().get("Content-Type");
            if(contentType != null){
                contentType = contentType.split(";")[0];
            }
            else if(this.getAcceptableContentType() != null && !this.getAcceptableContentType().startsWith("*")){
                contentType = this.getAcceptableContentType();
            }          
            if(contentType != null){
                contentType = contentType.toLowerCase();
            }
            
            /* render content */
            if (result instanceof String){ //String -> append to body
                if(contentType == null){   
                    contentType = "text/plain";
                }                
                this.getResponseBody().append(result);
            }           
            else if(result instanceof JsonObject){
                if(contentType == null){
                    contentType = "application/json";
                }               
                this.getResponseBody().append(result.toString());
            }
            else if(result instanceof Template){ //template -> render
                if(contentType == null){
                    contentType = "text/html";
                }
                TemplateEngine engine = c.getApplication().getConfig().getTemplateEngine();
                if(engine != null){
                  Template template = (Template) result;              
                  engine.render(ctx, template.getName(), res -> {
                    if (res.succeeded()) {
                      this.getResponseBody().append(res.result());
                    }
                    else {
                      ctx.fail(res.cause());
                    }
                  });
                }
                else{
                    logger.error("no template engine found");
                }
            }
            else { //object -> render based on content type (fallback json)
                if(contentType == null){
                    contentType = "application/json";
                }        
                if(contentType.endsWith("json")){
                    this.getResponseBody().append(WXTools.toJSON(result));
                }               
                else if(contentType.endsWith("xml")){
                    this.getResponseBody().append(WXTools.toXML(result, c.getJaxbContext()));                    
                }
                else if(contentType.endsWith("plain")){
                    this.getResponseBody().append(String.valueOf(result));   
                }
                else{
                    throw new HandleResponseException("Cannot serialize content type [" + contentType + "]");
                }
            }          
            
            /* set content type header , if not present */
            if(this.getResponse().headers().get("Content-Type") == null){
                this.getResponse().putHeader("Content-Type", contentType+"; charset=utf-8");    
            }     
        }catch(Exception ex){
            throw new HandleResponseException("cant handle response", ex);
        }   
        
   
        return this;
    }
    
    
    
    
}
