package io.wx.core3.http.cookies;

import io.vertx.core.http.HttpServerRequest;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;


/**
 * 
 * cookie managment
 *
 * @author moritz
 */
public class CookieStore {
    private static final Logger logger = LogManager.getLogger(CookieStore.class); 
    private final Map<String , String> cookies = new HashMap<>();
    private final Map<String, String> pushCookieValues = new HashMap<>();
    private HttpServerRequest req = null;

    public CookieStore(HttpServerRequest req){
        this.req = req;
        initCookieStore(req.headers().get("cookie"));
    }
    
    
    public Set<String> getCookieNames(){
        return cookies.keySet();
    }
    
    
    /***
     * initializes the cookie store based on a "cookie"-header from the httpclient
     * 
     * @param data 
     */
    private void initCookieStore(String data){
        if(data!=null){
            for(String pairs : data.split(";")){
                String[] param = pairs.split("=");
                if(param.length>1){
                    this.cookies.put(param[0].trim(), param[1]);
                } 
            }
        }        
    }
    
    
    /**
     *  returns a string version of the cookiestore, as it would be send by a http client as cookie header
     * @return 
     */
    public String toString(){
        StringBuilder  b = new StringBuilder();
        for(String pname : cookies.keySet()){
            if(b.length()>0){
                b.append("; ");
            }
            b.append(pname.trim()).append("=").append(cookies.get(pname));
        }
        return b.toString();
    }
    
    
    public String getValue(String name){
        return this.cookies.get(name);
    }
    
    public void setCookie(String name, String value){
        cookies.put(name, value);
        this.pushCookieValues.put(name, name+"="+value+"; path=/; ");
    }
    
    /*
    * sets the cookie to exired on client side and removes it the store
    */
    public void deleteCookie(String name){
        cookies.remove(name);
        this.pushCookieValues.put(name, name+"=null; path=/; expires=Thu, 01 Jan 1970 00:00:00 GMT");
    }
    
  
    
    /**
     * writes all new cookies to the header, with path=/;
    */
    public void toHeader(){
        //may not set this always?
        this.req.response().putHeader("Set-Cookie", this.pushCookieValues.values());
    }
    
    
}
