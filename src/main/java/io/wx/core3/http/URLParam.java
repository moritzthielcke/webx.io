/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package io.wx.core3.http;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 
 * URLParam annotation
 * 
 * As default we take the param name.However, .class files do not store formal parameter names by default.
 * To store formal parameter names in a particular .class file, 
 * and thus enable the Reflection API to retrieve formal parameter names, 
 * compile the source file with the -parameters option to the javac compiler.
 * 
 * @author Jose Luis Conde Linares-  jlinares@aptly.de
 */
@Retention( RetentionPolicy.RUNTIME )
@Target(ElementType.PARAMETER)
public @interface URLParam {
  String name() default "";
}
