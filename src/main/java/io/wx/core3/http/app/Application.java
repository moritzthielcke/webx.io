/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package io.wx.core3.http.app;

import com.google.inject.Injector;
import com.google.inject.Module;
import io.vertx.core.AsyncResult;
import io.vertx.core.CompositeFuture;
import io.vertx.core.Handler;
import io.vertx.core.http.HttpServerRequest;
import io.wx.core3.config.AppConfig;
import io.wx.core3.http.Core;
import io.wx.core3.security.Encrypter;
import java.io.File;
import java.util.Map;

/**
 *
 * @author moritz
 */
public interface Application {

    /****
     * return the classpath of your application
     * @return
     */
    String getApplicationPath();

    /***
     * returns the Application config
     * @return
     */
    AppConfig getConfig();

    /**
     * returns the webx core
     *
     * @return
     */
    Core getCore();

    /***
     * smart getter for encrypter, creates if not yet exists
     * @return
     * @throws Exception
     */
    Encrypter getEncrypter() throws Exception;

    /**
     * define your guice modules here
     */
    Map<String, ? extends Module> getModules();

    /**
     * define the handler used any requests which doesnt match a route
     * @return
     */
    Handler<HttpServerRequest> getNotFoundHandler();

    /***
     * returns the root injector
     * @return
     */
    Injector getRootInjector();

    /**
     * returns the SSL keystore
     * @return
     */
    File getSSLKeystore();

    /***
     * returns the ssl truststore
     * @return
     */
    File getSSLTrustStore();

    /*
     * idles the application so that the JVM doesnt shut down 
     */
    void idle() throws InterruptedException;

    /**
     * initializes the application, creates the core and the DI. after this happens, new entries in this.modules will not affect the di anymore
     * @return false if the application was already initialized, true its just happend
     */
    boolean initializeDI();

    /*
     * This function get called after the server(s) are all up and running.
     * At this point, the DI is available
     */
    void postStart(AsyncResult<CompositeFuture> ar);

    /***
     * sets the Application
     * @param config
     */
    void setConfig(AppConfig config);

    /*
     * starts the server, initializes the DI if it has not happend yet
     */
    Application start() throws Exception;
    
}
