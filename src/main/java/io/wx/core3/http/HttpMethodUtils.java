/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package io.wx.core3.http;

import io.wx.core3.http.routing.ControllerBlueprint;
import java.lang.reflect.Method;

/**
 *
 * @todo naming ?
 * @author Jose Luis Conde Linares-  jlinares@aptly.de
 */
public class HttpMethodUtils {
  public static final String EMPTY_PATH = "^\\/?$";

  private final Resource resource;
  private boolean httpMethod = true;
  private boolean httpResource = false;
  private ControllerBlueprint blueprint;

  public HttpMethodUtils(Class c, Method m, Core core) {
    Object methodAnnotation;
    resource = (Resource) c.getAnnotation(Resource.class);
    if ((methodAnnotation = m.getAnnotation(RequestMapping.class)) != null) {
      RequestMapping annotation = (RequestMapping) methodAnnotation;
      blueprint = new ControllerBlueprint(c, m, core, annotation);
    } 
    else if( m.getAnnotation(Resource.class) != null ){
      httpMethod = false;
      httpResource = true;
    }
    else {
      httpMethod = false;
    }
  }

  public boolean IsHttpMethod() {
    return httpMethod;
  }

  public boolean IsHttpResource() {
    return httpResource;
  }
  
  public ControllerBlueprint getBlueprint() {
    return blueprint;
  }

  public Resource getResource() {
    return resource;
  }

}
