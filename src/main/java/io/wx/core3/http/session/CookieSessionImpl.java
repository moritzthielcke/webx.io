/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package io.wx.core3.http.session;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import io.vertx.ext.web.Session;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * full featured session impl ( cache based )
 * @author moritz
 */
@JsonIgnoreProperties
public class CookieSessionImpl implements Session{
    private static Logger logger = LogManager.getLogger(CookieSessionImpl.class);
    private long ttl;
    public Map<String, Object> data = new HashMap();    
    public String ID;
    private String oldID;    
    private boolean regenerated=false;

    public CookieSessionImpl() {
        this(1000*60, null);
    }

    public CookieSessionImpl(long ttl, String id) {
        this.ttl = ttl;
        this.ID = id;
        if(this.ID == null){
            this.ID = generateID();
        }
    }
    
    @Override
    public String toString() {
        return "Session{" + "ID=" + ID + '}'+data;
    }
    
    
    public void clear(){
        this.data = new HashMap();
    }


    @Override
    public String id() {
        return ID;
    }

    @Override
    public Session put(String string, Object o) {
        data.put(string, o);
        return this;
    }

    @Override
    public <T> T get(String string) {
        return (T) data.get(string);
    }

    @Override
    public <T> T remove(String string) {
        return (T) data.remove(string);
    }

    @Override
    public Map<String, Object> data() {
        return this.data;
    }

    @Override
    public long lastAccessed() {
        logger.error("unsupported session call lastAccessed()");
        return new Date().getTime();
    }

    @Override
    public void destroy() {
        logger.debug("session destroy call"); //todo make debug
        this.data = new HashMap();
        this.ID = generateID();
    }

    @Override
    public boolean isDestroyed() {
        //@todo check if this causes problems
        if(ID == null && this.data().isEmpty()){
            return true;
        }
        return false;
    }

    @Override
    public long timeout() {
        return ttl;
    }

    @Override
    public void setAccessed() {
        logger.info("setAccessed() call");
    }
    

    private String generateID(){
        return UUID.randomUUID().toString().replace("-", "");
    }    

    @Override
    public String oldId() {
       return this.oldID;
    }

    @Override
    public Session regenerateId() {
        this.oldID = this.ID;
        this.ID = generateID();
        this.regenerated = true;
        return this;
    }

    @Override
    public boolean isRegenerated() {
        return this.regenerated;
    }

    
}
