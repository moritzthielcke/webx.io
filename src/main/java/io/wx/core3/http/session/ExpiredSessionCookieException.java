/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package io.wx.core3.http.session;

/**
 *
 * @author moritz
 */
public class ExpiredSessionCookieException extends Exception {

    /**
     * Creates a new instance of
     * <code>InvalidSessionCookieSignatureException</code> without detail
     * message.
     */
    public ExpiredSessionCookieException() {
    }

    /**
     * Constructs an instance of
     * <code>InvalidSessionCookieSignatureException</code> with the specified
     * detail message.
     *
     * @param msg the detail message.
     */
    public ExpiredSessionCookieException(String msg) {
        super(msg);
    }
    
    public ExpiredSessionCookieException(String msg, Throwable cause) {
        super(msg,cause);
    }
}
