/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package io.wx.core3.http.routing;

/**
 *
 * @author Jose Luis Conde Linares-  jlinares@aptly.de
 */
public interface RoutingOrder {

  public enum Order { 

    SYSTEM(-999999),
    TRAIT_CLASS_BEFORE(-888888),
    TRAIT_CLASS_AFTER(888888),
    TRAIT_METHOD_BEFORE(-777777),
    TRAIT_METHOD_AFTER(777777);

    private final int value;

    Order(int value) {
      this.value = value;
    }

    public int getValue() {
      return this.value;
    }

  }
}
