/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package io.wx.core3.http.routing;

import com.google.inject.Injector;
import io.vertx.ext.web.RoutingContext;
import io.wx.core3.http.Core;
import io.wx.core3.http.RequestControllerImpl;
import io.wx.core3.di.RequestControllerModule;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * binds some crucial objects to the routingcontext
 * 
 * @todo currently not used?
 * 
 * @author moritz
 */
@Deprecated
public class RoutingContextHelper {
    private static final Logger logger = LogManager.getLogger(RoutingContextHelper.class);
    private final RoutingContext ctx;
    private final Core core;
    private final Injector injector;
    private final RequestControllerImpl requestController;
    
   private RoutingContextHelper(RoutingContext e, Core c){
       this.ctx = e;
       this.core = c;
        if( e.data().get("_isdebug") == null){
            e.data().put("_isdebug", core.getApplication().getConfig().isDEVMODE());
            logger.info("setting debug to "+core.getApplication().getConfig().isDEVMODE());
        }        
        this.requestController = new RequestControllerImpl(e, core);
        this.injector = core.getRootInjector().createChildInjector(new RequestControllerModule(requestController));        
   }
   
   /***
    * returns the contexthelper for the given RoutingContext.
    * those RoutingContext-objects are bound to the current user-request (not static as it maybe appears)
     * @param e
     * @param c
     * @return 
    */
   public static RoutingContextHelper getInstance(RoutingContext e, Core c){ 
       logger.debug("getting helper for context "+e);
       if( e.data().get("_rqCtxHelper") == null){
           logger.debug("creating helper for context "+e);
           e.data().put("_rqCtxHelper", new RoutingContextHelper(e,c));
       }
       return (RoutingContextHelper)e.data().get("_rqCtxHelper");
   }

   /***
    * 
    * @return the dependency injector for  this request, contains RequestController
    */
   public Injector getInjector() {
        return injector;
    }


   /***
    * 
    * @return the request controller for this request
    */
    public RequestControllerImpl getRequestController() {
        return requestController;
    }
   


    public RoutingContext getCtx() {
        return ctx;
    }
   
   
    
}
