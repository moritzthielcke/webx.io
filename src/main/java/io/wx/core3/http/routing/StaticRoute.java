/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package io.wx.core3.http.routing;

/**
 *
 * @author Jose Luis Conde Linares - jlinares@aptly.de
 */
public class StaticRoute {
    private String path;
    private String directory;
    private Boolean isRegexp;

    public StaticRoute(String path, String directory) {
        this.path = path;
        this.directory = directory;
        this.isRegexp = false;
    }

    public StaticRoute(String path, String directory, Boolean isRegexp) {
        this.path = path;
        this.directory = directory;
        this.isRegexp = isRegexp;
    }
    
    public String getPath() {
        return path;
    }

    public StaticRoute setPath(String path) {
        this.path = path;
        return this;
    }

    public String getDirectory() {
        return directory;
    }

    public StaticRoute setDirectory(String directory) {
        this.directory = directory;
        return this;
    }

    public Boolean isRegexp() {
        return isRegexp;
    }

    public StaticRoute isRegexp(Boolean isRegexp) {
        this.isRegexp = isRegexp;
        return this;
    }
    
    
}
