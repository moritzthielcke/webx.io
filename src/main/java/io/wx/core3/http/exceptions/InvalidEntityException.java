/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package io.wx.core3.http.exceptions;

/**
 *
 * @author moritz
 */
@StatusCode(value = 400)
public class InvalidEntityException extends Exception {

    /**
     * Creates a new instance of <code>InvocationException</code> without detail
     * message.
     */
    public InvalidEntityException() {
    }

    /**
     * Constructs an instance of <code>InvocationException</code> with the
     * specified detail message.
     *
     * @param msg the detail message.
     */
    public InvalidEntityException(String msg) {
        super(msg);
    }
    
    public InvalidEntityException(String msg, Throwable ex) {
        super(msg, ex);
    }


}
