/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package io.wx.core3.http.exceptions;

import java.util.HashSet;
import java.util.Set;
import javax.validation.ConstraintViolation;

/**
 *
 * @author moritz
 */
@StatusCode(value = 400)
public class ValidationException extends Exception {
    private final Set<ConstraintViolation<Object>> violations;

    
    /**
     *
     * @param violations
     */
    public ValidationException(Set<ConstraintViolation<Object>> violations) {
        super();
        this.violations = violations;
    }
    
    public ValidationException(Throwable cause){
        super(cause);
        violations = new HashSet<>();
    }

    public ValidationException(){
       super();
       violations = new HashSet<>();
    }

    /**
     *
     * @param violations
     */
    public ValidationException(Set<ConstraintViolation<Object>> violations, String msg) {
        super(msg);
        this.violations = violations;
    }

    public ValidationException(Set<ConstraintViolation<Object>> violations, String msg, Throwable ex) {
        super(msg, ex);
        this.violations = violations;
    }
    
    public ValidationException(String msg){
        super(msg);
        violations = new HashSet<>();
    }    
    
    
    public Set<ConstraintViolation<Object>> getViolations() {
        return violations;
    }
    
    

}
