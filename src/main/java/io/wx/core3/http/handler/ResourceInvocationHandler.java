package io.wx.core3.http.handler;

import io.vertx.core.http.HttpMethod;
import io.vertx.ext.web.Session;
import io.wx.core3.http.URLParam;
import io.wx.core3.http.URLParamConsumer;
import io.wx.core3.http.exceptions.ValidationException;
import io.wx.core3.common.WXTools;
import io.wx.core3.common.Map2BeanConsumer;
import io.wx.core3.http.RequestController;
import io.wx.core3.http.exceptions.InvalidEntityException;
import io.wx.core3.http.exceptions.InvocationException;
import io.wx.core3.http.exceptions.StatusCode;
import io.wx.core3.http.routing.ControllerBlueprint;
import java.io.IOException;
import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.util.Set;
import javax.inject.Inject;
import javax.validation.ConstraintViolation;
import javax.validation.Validator;
import org.apache.commons.beanutils.ConvertUtils;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;

/**
 * handles the actuall invokation of your method over http
 *
 * @author moritz
 */
public class ResourceInvocationHandler extends Handler {
    private static final Logger logger = LogManager.getLogger(ResourceInvocationHandler.class);
    private final ControllerBlueprint blueprint;
    @Inject
    Validator validator;
    @Inject
    RequestController ctr;

    
    /**
     * *
     *
     * @param blueprint the blueprint describing your method
     */
    public ResourceInvocationHandler(ControllerBlueprint blueprint) {
        super(blueprint.getCore());
        this.blueprint = blueprint;
    }

    
    @Override
    public void handle() {
        ctr.doMatch(); //count's the matching controller

        /* get session if exists */
        Session session = ctr.getSession();

        //lets see if the controller was already invoked in this request, if yes load state
        String endpointIdent = "__controller_" + blueprint.getImpl().getName();
        Object scopedEndPointCtr = ctr.getData().get(endpointIdent);
        Object endPointCtr = null; //the controller implementation
        if (scopedEndPointCtr != null) {
            logger.debug("loading " + endpointIdent);
            endPointCtr = scopedEndPointCtr;
        } else {
            //load controller state from session 
            if (session != null) {
                if (blueprint.getStatefulID() != null) {
                    logger.debug("loading stateful ctr from session " + blueprint.getStatefulID());
                    endPointCtr = session.get(blueprint.getStatefulID()); //@todo exception handling? probably not needed here, crashes later :) 
                }
            }
            try {
                if (endPointCtr == null) {
                    logger.debug("creating new controller");
                    endPointCtr = blueprint.getImpl().newInstance();
                }
                //inject stateful fields
                if (session != null && null != blueprint.getStatefulFields()) {
                    for (Field f : blueprint.getStatefulFields()) {
                        Object value = session.get(endPointCtr.getClass().getName() + '@' + f.getName());
                        if (null != value) {
                            Boolean accessible = f.isAccessible();
                            f.setAccessible(true);
                            f.set(endPointCtr, value);
                            f.setAccessible(accessible);
                        }
                    }
                }
                //inject the http controller to resolve its dependencies   
                getInjector().injectMembers(endPointCtr);
            } catch (Exception ex) {
                logger.error("cant init controller class " + blueprint.getImpl().getName(), ex);
                this.ctr.fail(new InvocationException("internal server error", ex));
                return;
            }
        }
        //invoke endpoint
        try {
            //fill method params:
            Object[] invokeParams;
            invokeParams = getIvokeParams(blueprint, this.ctr);//param array for the method to call                        
            
            //setting the defined responseStatus by the annotation 
            this.ctr.getResponse().setStatusCode(blueprint.getHttpStatus().value());
            
            //invoke method
            Object result = blueprint.getMethod().invoke(endPointCtr, invokeParams);
            //store state of ctr in requestscope:
            ctr.getData().put(endpointIdent, endPointCtr);
            //after the invocation we need to save the state of the controller is it's stateful
            if (session != null) {
                if (blueprint.getStatefulID() != null) {
                    session.put(blueprint.getStatefulID(), endPointCtr);
                    logger.debug("pushing " + blueprint.getStatefulID() + " => " + session);
                } else if (null != blueprint.getStatefulFields()) {
                    String ctrlName = endPointCtr.getClass().getName();
                    for (Field f : blueprint.getStatefulFields()) {
                        try {
                            Object value = f.get(endPointCtr);
                            String sessionKey = ctrlName + '@' + f.getName();
                            session.put(sessionKey, value);
                        } catch (Exception ex) {
                            logger.error(ex);
                            throw new IllegalStateException(ex); //@todo replace with custom exception pls?
                        }
                    }
                }
            }
            //if the invoked method does not return void, we want to handle the response for the user and call next
            if (!blueprint.getMethod().getReturnType().equals(Void.TYPE)) {
                if (result != null) {
                    this.ctr.doResponse(result);
                }
                this.ctr.next();
            }
        } catch (Throwable ex) {
            //this one is tricky: the invocationTarget exception is wrapping the actually thrown exception already....:D so we need the cause:
            if(ex instanceof InvocationTargetException){
                ex = ex.getCause();
            }        
            if(!ex.getClass().isAnnotationPresent(StatusCode.class)){
                //if an exception without any statuscode is thrown by an invocation , its probably unexpected , 
                // wrap it an the InvocationException , will make the default last handler log it (since it's an 500)
                ex = new InvocationException("internal server error", ex); 
            }
            this.ctr.fail(ex);
        }
    }
    

    /**
     * *
     *
     * maps http request params to method params
     *
     * @param blueprint
     * @param requestController
     * @return array of method params based on the request
     * @throws InvocationException
     * @throws ValidationException
     */
    public Object[] getIvokeParams(ControllerBlueprint blueprint, RequestController requestController) throws InvocationException, ValidationException, InvalidEntityException {
        int paramSize = blueprint.getMethod().getParameterTypes().length;
        Object[] invokeParams = new Object[paramSize]; //param array for the method to call        
        Class[] parameterTypes = blueprint.getMethod().getParameterTypes(); //parameter types
        Annotation[][] parameterAnnotations = blueprint.getMethod().getParameterAnnotations(); //parameter type annotation
        if (paramSize > 0) {
            boolean bodyConsumed = false; //currently we use the whole body for "the first parameter" that isnt a URLParam....
            for (int i = 0; i < paramSize; i++) {
                String describeParam = "method Param " + (i + 1) + " of " + blueprint.getMethod();
                //is url param?
                boolean isURLParam = false;
                for (Annotation pa : parameterAnnotations[i]) {
                    if (pa instanceof URLParamConsumer) {
                        isURLParam = true;
                        //create bean:
                        Map2BeanConsumer paramConsumer;
                        try {
                            paramConsumer = new Map2BeanConsumer(parameterTypes[i].newInstance());
                        } catch (InstantiationException | IllegalAccessException ex) {
                            throw new InvocationException("cant read URL params", ex);
                        }
                        paramConsumer.setDebug(requestController.isDebug());
                        requestController.getRequest().params().forEach(paramConsumer);
                        if (!WXTools.containsOptional(parameterAnnotations[i])) {
                            //validate
                            Set<ConstraintViolation<Object>> violations = validator.validate(paramConsumer.getObject());
                            if (violations.size() > 0) {
                                throw new ValidationException(violations, "invalid request");
                            }
                        }
                        invokeParams[i] = paramConsumer.getObject();
                    } else if (pa instanceof URLParam) {
                        URLParam param = (URLParam) pa;
                        String paramName = param.name();
                        //As deafult we take the param name.However, .class files do not store formal parameter names by default.
                        //To store formal parameter names in a particular .class file, 
                        //and thus enable the Reflection API to retrieve formal parameter names, 
                        //compile the source file with the -parameters option to the javac compiler.
                        if (paramName.equals("")) {
                            paramName = blueprint.getMethod().getParameters()[i].getName();
                        }
                        //casting
                        Object convert = ConvertUtils.convert(requestController.getRequest().params().get(paramName), blueprint.getMethod().getParameters()[i].getType());
                        invokeParams[i] = convert;
                        isURLParam = true;
                    }
                }
                //not a URL Param, check if we have to read the body
                if (!isURLParam && blueprint.getHttpMethod() != HttpMethod.GET
                        && blueprint.getHttpMethod() != HttpMethod.DELETE) {
                    if (!bodyConsumed) {
                        Boolean optional = WXTools.containsOptional(parameterAnnotations[i]);
                        try {
                            bodyConsumed = true;
                            if (requestController.getRequestBodyAsString() == null) {
                                logger.error("request body is null, method parameter will be null. Make sure you added a bodyhandler to the request!");
                                throw new IOException("cant read request body");
                            } else {
                                invokeParams[i] = WXTools.mapper.readValue(requestController.getRequestBodyAsString(), parameterTypes[i]);
                                if (!optional) {
                                    Set<ConstraintViolation<Object>> violations = validator.validate(invokeParams[i]);
                                    if (violations.size() > 0) {
                                        throw new ValidationException(violations, "invalid request");
                                    }
                                }
                            }
                        } catch (IOException ex) {
                            if (requestController.isDebug()) {
                                logger.error("cant parse request body ", ex);
                            }
                            if (!optional) {
                                throw new InvalidEntityException("invalid entity in request body",ex); // @todo better error message?
                                //throw new InvocationException("invalid entity", ex).setStatusCode(400);
                            }
                        }
                    } //unmapped parameter , just null it
                    else {
                        invokeParams[i] = null;
                        if (requestController.isDebug()) {
                            logger.warn("ignoring parameter " + blueprint.getImpl() + ": " + describeParam);
                        }
                    }
                }
            }
        }
        return invokeParams;
    }

}
