/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package io.wx.core3.http.handler;

import io.vertx.core.Handler;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.RoutingContext;
import io.wx.core3.http.exceptions.StatusCode;
import io.wx.core3.http.exceptions.ValidationException;
import javax.validation.ConstraintViolation;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 *
 * @author moritz
 */
public class DefaultFailureHandler implements Handler<RoutingContext>{
   private static Logger logger = LogManager.getLogger(DefaultFailureHandler.class); 
    
    @Override
    public void handle(RoutingContext e) {
       //logger.info("default error handler taking care of your stuff "+e.normalisedPath()+" -> "+e.statusCode());
       Boolean isDebug = e.data().get("_isdebug") != null ? (Boolean)e.data().get("_isdebug") : false; //@todo app default value required here is no match?
       if(e.failed()){
            Throwable ex = (Throwable) e.failure(); //@todo is this save?           
            int statusCode = e.statusCode() != -1 ? e.statusCode() : 500;
            if(ex != null && ex.getClass().isAnnotationPresent(StatusCode.class)){
                statusCode = ex.getClass().getAnnotation(StatusCode.class).value();
            }            
            if(statusCode == 500){
                logger.error("internal server error",ex);
            }            
            String msg = (ex != null && ex.getMessage() != null) ? ex.getMessage() : "ups";
            //build response
            JsonObject error = new JsonObject();
            error.put("error", msg);
            if(ex != null && isDebug ){ 
                JsonObject myException = new JsonObject();
                String exceptionMsg = ex.getMessage() != null ? ex.getMessage() : msg;
                myException.put("message", exceptionMsg);
                myException.put("type", ex.getClass().getSimpleName());
                error.put("exception", myException); 
                if(ex.getCause()!=null){
                    JsonObject cause = new JsonObject();
                    cause.put("message", ex.getCause().getMessage());
                    cause.put("type", ex.getCause().toString());
                    JsonArray stcktrc = new JsonArray();
                    for(StackTraceElement trace : ex.getCause().getStackTrace() ){
                        stcktrc.add(trace.toString());
                    }        
                    cause.put("stacktrace", stcktrc);
                    error.put("cause", cause);
                }    
                JsonArray stcktrc = new JsonArray();
                for(StackTraceElement trace : ex.getStackTrace()){
                    stcktrc.add(trace.toString());
                }
                myException.put("stacktrace", stcktrc);
            }
            if(ex != null && ex instanceof ValidationException ){
                JsonArray violations = new JsonArray();
                for( ConstraintViolation v : ((ValidationException)ex).getViolations() ){
                    JsonObject vDetails = new JsonObject();
                    vDetails.put("field", v.getPropertyPath().toString());
                    vDetails.put("error", v.getMessage());
                    violations.add(vDetails);
                }
                error.put("validationErrors", violations);
            }
            e.response().setStatusCode(statusCode);
            e.response().putHeader("Content-Type", "application/json; charset=utf-8");
            if(ex!=null){
               e.response().end(error.toString());   
            }else{
                if(e.statusCode()==404){
                    e.response().end("Not found");
                }
                else{
                    e.response().end(); //need msg's for error codes now?:|
                }
            }
       }
       else{
           logger.error("failure handler called with failure?!");
       }
    }

   
}
