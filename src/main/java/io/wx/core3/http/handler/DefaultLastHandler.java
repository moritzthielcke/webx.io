/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package io.wx.core3.http.handler;

import io.vertx.ext.web.RoutingContext;
import io.wx.core3.http.Core;
import io.wx.core3.http.exceptions.InvocationException;
import io.wx.core3.http.RequestController;
import javax.inject.Inject;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * the handler which is , by default, called last when using next()
 * writes the response body to the reponse, so if you dont want that to happen  end your response yourself
 * @author moritz
 */
public class DefaultLastHandler extends Handler{
    private static final Logger logger = LogManager.getLogger(DefaultLastHandler.class); 
    @Inject public RequestController ctr;

    public DefaultLastHandler(Core c) {
        super(c);
    }

    @Override
    public void handle() {
        RoutingContext e = ctr.getRoutingContext();
        logger.debug("endhandler call");
        //@todo is chunked?? Chunked encoding allows the sender to send additional header fields after the message body. 
        if(!e.response().ended()){  
            if(e.get("_routematches") == null){
                //this route has no matches of RequestControllerInvocationHandler so we assume its a 404
                ctr.fail(404);
                return;
            }
                       

            ctr.saveSession();
            //get buffered response body
            String bodyAsString = e.data().get("_responsebody") != null ? ((StringBuilder) e.data().get("_responsebody")).toString() : "";   
            Boolean writeBody = (e.response().headers().get("Content-length") == null); //decides if we want to write the response body/content-length
            
            //txt html as default content type:
            //@todo specify default content type per config?
            if (e.response().headers().get("Content-Type") == null) {
                e.response().putHeader("Content-Type", "text/html; charset=utf-8");
            }
            
            //set no cache
            if(e.response().headers().get("Cache-Control") == null){
                e.response().putHeader("Cache-Control", "no-cache, no-store, must-revalidate");
            }
            
            //set content length
            if(writeBody){
                try{
                    e.response().putHeader("Content-length", String.valueOf(bodyAsString.getBytes("UTF-8").length));
                    e.response().write(bodyAsString);             
                }catch(Exception ex){
                    logger.error("Default Handler Exception: ",ex);
                    ctr.fail(new InvocationException("cant write response body", ex));
                }
            }
            else if(bodyAsString.length()>0){
                logger.warn("discarding responsebody, Content-length is already set!");
            }

            //send body to client:
            try {
                e.response().end();
            } catch (Exception ex) {
                logger.error("cant end response...dont end responses yourself dude, thats not cool");
            }
        }
    }

   
}
