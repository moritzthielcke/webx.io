package io.wx.core3.http;

import io.wx.core3.http.traits.TraitConf;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 *
 * http resource annotation
 * 
 * @author moritz
 */
@Retention( RetentionPolicy.RUNTIME )
public @interface Resource{
    String path();
    String consumes() default ""; 
    String[] produces() default {};  
    TraitConf[] traits() default {};
}
