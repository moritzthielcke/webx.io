/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package io.wx.core3.templates;

/**
 *
 * @author Jose Luis Conde Linares-  jlinares@aptly.de
 */
public class Template {
  private String name;

  public Template(String name) {
    this.name = name;
  }

  public String getName() {
    return name;
  }
  
}
