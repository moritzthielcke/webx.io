/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package io.wx.core3;

import io.vertx.core.http.HttpClient;
import io.vertx.core.http.HttpClientRequest;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.unit.Async;
import io.vertx.ext.unit.TestContext;
import io.vertx.ext.unit.junit.VertxUnitRunner;
import io.vertx.ext.web.templ.ThymeleafTemplateEngine;
import io.wx.core3.app.beans.User;
import io.wx.core3.common.WXTools;
import io.wx.core3.config.AppConfig;
import io.wx.core3.config.SimpleConfig;
import io.wx.core3.http.HttpStatus;
import java.net.HttpCookie;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;

/**
 *
 * @author Jose Luis Conde Linares-  jlinares@aptly.de
 */
@RunWith(VertxUnitRunner.class)
public class BasicTest {

  public static MyApplication myApp;
  public static String RESOURCE_URL = "/hello";

  @BeforeClass
  public static void setUpApplication() throws Exception {
    AppConfig cfg = new SimpleConfig().setPort(1337).setInstanceCount(1)
                                          .setStage(AppConfig.Stage.LOCAL)
                                          .setTemplateEngine(ThymeleafTemplateEngine.create());
    cfg.getTemplateRoutes().add(".+\\.html"); 
    
    myApp = new MyApplication(cfg);
    myApp.start();
        Thread.sleep(1000);
  }

  @AfterClass
  public static void setDownApplication() throws Exception {
    myApp.getCore().getVertx().close();
    myApp = null;
  }
  
  @Test
  public void testOrder(TestContext context) {
    String expected = "HelloWorld->hello world 2 --> 1 HelloWorld->hello world 1 --> 2 HelloWorld->hello world 3 --> 3 HelloWorld->hello world JSON --> 4";
    Async async1 = context.async();
    HttpClient client = myApp.getCore().getVertx().createHttpClient();
    HttpClientRequest req = client.get(myApp.getConfig().getPort(), myApp.getConfig().getHost(), RESOURCE_URL + "/world");
    req.exceptionHandler(err -> context.fail(err.getMessage()));
    req.handler(resp -> {
      context.assertEquals(200, resp.statusCode());
      resp.bodyHandler(body -> {
        context.assertEquals(expected, body.toString());
        async1.complete();
      });
    });
    req.end();
  }
  
  @Test
  public void testAsyncHandling(TestContext context) {
    String expected = "HelloWorld + hello void";
    Async async1 = context.async();
    HttpClient client = myApp.getCore().getVertx().createHttpClient();
    HttpClientRequest req = client.get(myApp.getConfig().getPort(), myApp.getConfig().getHost(), RESOURCE_URL + "/void");
    req.exceptionHandler(err -> context.fail(err.getMessage()));
    req.handler(resp -> {
      context.assertEquals(200, resp.statusCode());
      resp.bodyHandler(body -> {
        context.assertEquals(expected, body.toString());
        async1.complete();
      });
    });
    req.end();
  }
  
  @Test
  public void testError(TestContext context) {
    String expected = "oh man";
    Async async1 = context.async();
    HttpClient client = myApp.getCore().getVertx().createHttpClient();
    HttpClientRequest req = client.get(myApp.getConfig().getPort(), myApp.getConfig().getHost(), RESOURCE_URL + "/error");
    req.exceptionHandler(err -> context.fail(err.getMessage()));
    req.handler(resp -> {
      context.assertEquals(500, resp.statusCode());
      resp.bodyHandler(body -> {
        JsonObject error = new JsonObject(body.toString());
        context.assertEquals(expected, error.getString("error"));
        async1.complete();
      });
    });
    req.end();
  }
  
  @Test
  public void testException(TestContext context) {
    String expected = "internal server error";
    Async async1 = context.async();
    HttpClient client = myApp.getCore().getVertx().createHttpClient();
    HttpClientRequest req = client.get(myApp.getConfig().getPort(), myApp.getConfig().getHost(), RESOURCE_URL + "/exception");
    req.exceptionHandler(err -> context.fail(err.getMessage()));
    req.handler(resp -> {
      context.assertEquals(500, resp.statusCode());
      resp.bodyHandler(body -> {
        JsonObject error = new JsonObject(body.toString());
        context.assertEquals(expected, error.getString("error"));
        async1.complete();
      });
    });
    req.end();
  }
  
  @Test
  public void testGetUser(TestContext context) {
    String expected = "John";
    Async async1 = context.async();
    HttpClient client = myApp.getCore().getVertx().createHttpClient();
    HttpClientRequest req = client.get(myApp.getConfig().getPort(), myApp.getConfig().getHost(), RESOURCE_URL + "/user?username=John");
    req.exceptionHandler(err -> context.fail(err.getMessage()));
    req.handler(resp -> {
      context.assertEquals(200, resp.statusCode());
      resp.bodyHandler(body -> {
        JsonObject user = new JsonObject(body.toString());
        context.assertEquals(expected, user.getString("name"));
        async1.complete();
      });
    });
    req.end();
  }
  
  @Test
  public void testPutUser(TestContext context) {
    String expected = "John";
    User user = new User();
    user.setName(expected);
    Async async1 = context.async();
    HttpClient client = myApp.getCore().getVertx().createHttpClient();
    HttpClientRequest req = client.put(myApp.getConfig().getPort(), myApp.getConfig().getHost(), RESOURCE_URL + "/user");
    req.exceptionHandler(err -> context.fail(err.getMessage()));
    req.handler(resp -> {
      context.assertEquals(200, resp.statusCode());
      resp.bodyHandler(body -> {
        JsonObject json = new JsonObject(body.toString());
        context.assertEquals(expected, json.getString("name"));
        async1.complete();
      });
    });
    req.putHeader("content-type", "application/json");
    req.putHeader("content-length", "" + WXTools.toJSON(user).length());
    req.write(WXTools.toJSON(user));
    req.end();
  }
  
  @Test
  public void testPutBody(TestContext context) {
    User user = new User();
    user.setName("John");
    String expected = "-" + WXTools.toJSON(user);
    Async async1 = context.async();
    HttpClient client = myApp.getCore().getVertx().createHttpClient();
    HttpClientRequest req = client.put(myApp.getConfig().getPort(), myApp.getConfig().getHost(), RESOURCE_URL + "/body");
    req.exceptionHandler(err -> context.fail(err.getMessage()));
    req.handler(resp -> {
      context.assertEquals(200, resp.statusCode());
      resp.bodyHandler(body -> {
        context.assertEquals(expected, body.toString());
        async1.complete();
      });
    });
    req.putHeader("content-type", "application/json");
    req.putHeader("content-length", "" + WXTools.toJSON(user).length());
    req.write(WXTools.toJSON(user));
    req.end();
  }
  
  
  @Test
  public void testWriteRequestAfterSettingContentLength(TestContext context) {
    String expected = "hi write";
    Async async1 = context.async();
    HttpClient client = myApp.getCore().getVertx().createHttpClient();
    HttpClientRequest req = client.get(myApp.getConfig().getPort(), myApp.getConfig().getHost(), RESOURCE_URL + "/write");
    req.exceptionHandler(err -> context.fail(err.getMessage()));
    req.handler(resp -> {
      context.assertEquals(200, resp.statusCode());
      resp.bodyHandler(body -> {
        context.assertEquals(expected, body.toString());
        async1.complete();
      });
    });
    req.end();
  }
  
  @Test
  public void testStatefull(TestContext context) throws InterruptedException {
    String expected1 = "1";
    String expected2 = "2";
    String expectedAnotherClient = "1";
    StringBuffer cookies = new StringBuffer();
    Async async1 = context.async();
    
    HttpClient client = myApp.getCore().getVertx().createHttpClient();
    HttpClientRequest req = client.get(myApp.getConfig().getPort(), myApp.getConfig().getHost(), RESOURCE_URL + "/count");
    req.exceptionHandler(err -> context.fail(err.getMessage()));
    req.handler(resp -> {
      context.assertEquals(200, resp.statusCode());
      Integer numCookies = resp.cookies().size();
      for(int i = 0; i < numCookies ; i++){
        for(HttpCookie cookie : HttpCookie.parse(resp.cookies().get(i))){
          cookies.append(cookie.getName()).append("=").append(cookie.getValue()).append(";");
        }
      }
      resp.bodyHandler(body -> {
        context.assertEquals(expected1, body.toString());
        async1.complete();
      });
    });
    req.end();
    
    async1.awaitSuccess();
    
    Async async2 = context.async();
    req = client.get(myApp.getConfig().getPort(), myApp.getConfig().getHost(), RESOURCE_URL + "/count");
    req.handler(response -> {
      context.assertEquals(200, response.statusCode());
      response.bodyHandler(body -> {
        context.assertEquals(expected2, body.toString());
        async2.complete();
      });
    });
    req.putHeader("Cookie", cookies);
    req.end();

    Async async3 = context.async();
    req = client.get(myApp.getConfig().getPort(), myApp.getConfig().getHost(), RESOURCE_URL + "/count");
    req.handler(response -> {
      context.assertEquals(200, response.statusCode());
      response.bodyHandler(body2 -> {
        context.assertEquals(expectedAnotherClient, body2.toString());
        async3.complete();
      });
    });
    req.end();
  }
  
  @Test
  public void testResponseStatus(TestContext context) {
    int expected = HttpStatus.ACCEPTED.value();
    Async async = context.async();
    HttpClient client = myApp.getCore().getVertx().createHttpClient();
    HttpClientRequest req = client.get(myApp.getConfig().getPort(), myApp.getConfig().getHost(), RESOURCE_URL + "/httpStatus");
    req.exceptionHandler(err -> context.fail(err.getMessage()));
    req.handler(resp -> {
      context.assertEquals(expected, resp.statusCode());
      async.complete();
    });
    req.end();
  }
}
