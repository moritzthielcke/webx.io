/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package io.wx.core3;

import io.vertx.core.http.HttpClient;
import io.vertx.core.http.HttpClientRequest;
import io.vertx.ext.unit.Async;
import io.vertx.ext.unit.TestContext;
import io.vertx.ext.unit.junit.VertxUnitRunner;
import io.vertx.ext.web.templ.ThymeleafTemplateEngine;
import io.wx.core3.app.beans.ValidatedBean;
import io.wx.core3.common.WXTools;
import io.wx.core3.config.AppConfig;
import io.wx.core3.config.SimpleConfig;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;

/**
 *
 * @author Jose Luis Conde Linares - jlinares@aptly.de
 */
@RunWith(VertxUnitRunner.class)
public class ErrorCodeTest {

  public static MyApplication myApp;
  public static String RESOURCE_URL = "/exceptions";

  @BeforeClass
  public static void setUpApplication() throws Exception {
    AppConfig cfg = new SimpleConfig().setPort(1337).setInstanceCount(1)
                                      .setStage(AppConfig.Stage.LOCAL)
                                      .setTemplateEngine(ThymeleafTemplateEngine.create());
    cfg.getTemplateRoutes().add(".+\\.html");

    myApp = new MyApplication(cfg);
    myApp.start();
    Thread.sleep(1000);    
  }

  @AfterClass
  public static void setDownApplication() throws Exception {
    myApp.getCore().getVertx().close();
    myApp = null;
  }

  @Test
  public void testCustomException(TestContext context) {
    Integer expected = 403;
    Async async1 = context.async();
    HttpClient client = myApp.getCore().getVertx().createHttpClient();
    HttpClientRequest req = client.get(myApp.getConfig().getPort(), myApp.getConfig().getHost(), RESOURCE_URL);
    req.exceptionHandler(err -> context.fail(err.getMessage()));
    req.handler(resp -> {
      context.assertEquals(expected, resp.statusCode());
      async1.complete();
    });
    req.end();
  }

  @Test
  public void testAsyncException(TestContext context) {
    Integer expected = 404;
    Async async1 = context.async();
    HttpClient client = myApp.getCore().getVertx().createHttpClient();
    HttpClientRequest req = client.get(myApp.getConfig().getPort(), myApp.getConfig().getHost(), RESOURCE_URL + "/async");
    req.exceptionHandler(err -> context.fail(err.getMessage()));
    req.handler(resp -> {
      context.assertEquals(expected, resp.statusCode());
      async1.complete();
    });
    req.end();
  }

  @Test
  public void testNullpointerException(TestContext context) {
    Integer expected = 500;
    Async async1 = context.async();
    HttpClient client = myApp.getCore().getVertx().createHttpClient();
    HttpClientRequest req = client.get(myApp.getConfig().getPort(), myApp.getConfig().getHost(), RESOURCE_URL + "/nullpointer");
    req.exceptionHandler(err -> context.fail(err.getMessage()));
    req.handler(resp -> {
      context.assertEquals(expected, resp.statusCode());
      async1.complete();
    });
    req.end();
  }

  @Test
  public void testValidationException(TestContext context) {
    ValidatedBean bean = new ValidatedBean();
    bean.setUsername("username");
    bean.setPassword("password");
    Async async1 = context.async();
    HttpClient client = myApp.getCore().getVertx().createHttpClient();
    HttpClientRequest req = client.post(myApp.getConfig().getPort(), myApp.getConfig().getHost(), RESOURCE_URL + "/validate");
    req.exceptionHandler(err -> context.fail(err.getMessage()));
    req.handler(resp -> {
      context.assertEquals(200, resp.statusCode());
      async1.complete();

    });
    req.putHeader("content-type", "application/json");
    req.putHeader("content-length", "" + WXTools.toJSON(bean).length());
    req.write(WXTools.toJSON(bean));
    req.end();

    async1.awaitSuccess();

    bean.setUsername(null);
    Async async2 = context.async();
    req = client.post(myApp.getConfig().getPort(), myApp.getConfig().getHost(), RESOURCE_URL + "/validate");
    req.exceptionHandler(err -> context.fail(err.getMessage()));
    req.handler(resp -> {
      context.assertEquals(400, resp.statusCode());
      async2.complete();
    });
    req.putHeader("content-type", "application/json");
    req.putHeader("content-length", "" + WXTools.toJSON(bean).length());
    req.write(WXTools.toJSON(bean));
    req.end();
  }

}
