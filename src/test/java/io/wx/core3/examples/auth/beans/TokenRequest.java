/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package io.wx.core3.examples.auth.beans;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import javax.validation.constraints.NotNull;
import org.apache.bval.constraints.NotEmpty;

/**
 *
 * @author moritz
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class TokenRequest {
    @NotNull
    @NotEmpty
    private String grant_type;   
    private String username;
    private String password;
    @NotNull
    @NotEmpty    
    private String client_id;
    @NotNull
    @NotEmpty    
    private String client_secret;    
    private String refresh_token;

    public String getRefresh_token() {
        return refresh_token;
    }
    
    

    public String getGrant_type() {
        return grant_type;
    }

    public String getClient_id() {
        return client_id;
    }

    public String getClient_secret() {
        return client_secret;
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

    public TokenRequest setGrant_type(final String value) {
        this.grant_type = value;
        return this;
    }

    public void setUsername(final String value) {
        this.username = value;
     //   return this;
    }

    public TokenRequest setPassword(final String value) {
        this.password = value;
        return this;
    }

    public TokenRequest setClient_id(final String value) {
        this.client_id = value;
        return this;
    }

    public TokenRequest setClient_secret(final String value) {
        this.client_secret = value;
        return this;
    }

    public TokenRequest setRefresh_token(final String value) {
        this.refresh_token = value;
        return this;
    }

    
    
    
    
}
