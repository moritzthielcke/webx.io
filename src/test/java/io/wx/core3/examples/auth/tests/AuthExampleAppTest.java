/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package io.wx.core3.examples.auth.tests;


import io.vertx.core.AsyncResult;
import io.vertx.core.Future;
import io.vertx.core.Handler;
import io.vertx.core.buffer.Buffer;
import io.vertx.core.http.HttpClient;
import io.vertx.core.http.HttpClientOptions;
import io.vertx.core.http.HttpClientRequest;
import io.vertx.core.http.HttpClientResponse;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.auth.oauth2.AccessToken;
import io.vertx.ext.auth.oauth2.OAuth2Auth;
import io.vertx.ext.auth.oauth2.OAuth2ClientOptions;
import io.vertx.ext.auth.oauth2.OAuth2FlowType;
import io.vertx.ext.auth.oauth2.impl.AccessTokenImpl;
import io.vertx.ext.unit.Async;
import io.vertx.ext.unit.TestContext;
import io.vertx.ext.unit.junit.VertxUnitRunner;
import io.vertx.ext.web.templ.ThymeleafTemplateEngine;
import io.wx.core3.common.WXTools;
import io.wx.core3.config.AppConfig;
import io.wx.core3.config.SimpleConfig;
import io.wx.core3.examples.auth.AuthExampleApp;
import io.wx.core3.examples.auth.beans.TokenRequest;
import io.wx.core3.examples.auth.beans.TokenResponse;
import io.wx.core3.examples.auth.common.RequestAuthenticator;
import io.wx.core3.http.app.AbstractApplication;
import java.io.IOException;
import java.util.concurrent.CompletableFuture;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;

/**
 *
 * @author moritz
 */
@RunWith(VertxUnitRunner.class)
public class AuthExampleAppTest {
    private static Logger logger = LogManager.getLogger(AuthExampleAppTest.class);
    private static String site; //base url of site
    private static OAuth2ClientOptions credentials;
    private static JsonObject tokenConfig;
    public static AbstractApplication app;
    public static HttpClient client; 


    @BeforeClass
    public static void setUpApplication() throws Exception {
        //Configure and create Application
        AppConfig cfg = new SimpleConfig().setPort(1337).setInstanceCount(3)
                                          .setStage(AppConfig.Stage.TEST)
                                          .setTemplateEngine(ThymeleafTemplateEngine.create());    
        app = new AuthExampleApp(cfg);      
        //configure and create http client
        client = app.getCore().getVertx().createHttpClient( new HttpClientOptions()
                                                            .setDefaultHost(cfg.getHost())
                                                            .setDefaultPort(cfg.getPort())
                                                            .setConnectTimeout(500)
                                                            .setMaxPoolSize(5));           
        //Start app and give it 0.5 seconds to rampup the http servers
        app.start();       
        Thread.sleep(500);
        //configure rest of the test env:
        site = "http://"+app.getConfig().getHost()+":"+app.getConfig().getPort();    
        credentials = new OAuth2ClientOptions()
                        .setClientID("client_id")
                        .setClientSecret("client_secret")
                        .setSite(site)
                        .setTokenPath("/auth/token");        
        tokenConfig = new JsonObject()
                                .put("username", "Hans")
                                .put("password", "password")
                                .put("client_id","client_id")
                                .put("client_secret", "client_secret");            
    }   
  
    @AfterClass
    public static void setDownApplication() throws Exception {
        app.getCore().getVertx().close(); //@todo implement AbstractApplication.shutDown();
        app = null;
    }  
  

    @Test
    public void testRequestAuthenticator(TestContext ctx){
        final Async asyncTest = ctx.async();  
        
        // your "future" plan
        Future<HttpClientResponse> getPage = Future.future();
        getPage.setHandler((AsyncResult<HttpClientResponse> e) -> {
            if(e.succeeded()){
                logger.info("got server response "+e.result().statusCode());
                e.result().bodyHandler((Buffer e1) -> {
                    logger.info("got body ,  "+e1);
                    asyncTest.complete();
                });
            }
            else{
                ctx.fail(e.cause());
            }
        });

        //do it:
        OAuth2Auth oauth2 = OAuth2Auth.create(app.getCore().getVertx(), OAuth2FlowType.PASSWORD, credentials);
        RequestAuthenticator authenticator = new RequestAuthenticator(oauth2, tokenConfig);    
        authenticator.doRequest( AuthExampleAppTest.client.get("/secured") ,
                                 getPage );
        
    }
      


    @Test
    public void testBodyHandling(TestContext ctx){
        final Async asyncTest = ctx.async(); 
        HttpClientRequest request = client.post("/auth/token", (HttpClientResponse e) -> {               
                e.bodyHandler(new Handler<Buffer>() {
                    @Override
                    public void handle(Buffer e) {
                        logger.info("buffer length: "+e.length()+" content -> "+e.toString());
                        asyncTest.complete();
                    }
                });
        });
        request.putHeader("content-type", "application/x-www-form-urlencoded");
        request.end("grant_type=password&username=hans&password=password&client_secret=client_secret&client_id=client_id");
    }
    
    

    
    @Test
    public void testOauth2(TestContext ctx){    
        final Async asyncTest = ctx.async(); 
        
        OAuth2Auth oauth2 = OAuth2Auth.create(app.getCore().getVertx(), OAuth2FlowType.PASSWORD, credentials);
        Future<AccessToken> gotAccessToken = Future.future();
        oauth2.getToken(tokenConfig, res -> {
            if(res.failed()){
                logger.info("failed!");
                gotAccessToken.fail(res.cause());
            }
            else{
                AccessToken token = res.result();
                logger.info(token.principal().toString());
                gotAccessToken.complete(res.result());
            }
        });
        
        gotAccessToken.setHandler((AsyncResult<AccessToken> e) -> {
            if(e.failed()){
                ctx.fail(e.cause());
            }
            else{
                HttpClientRequest request = AuthExampleAppTest.client.get("/secured");           
                request.handler((HttpClientResponse r) -> {
                    if(r.statusCode()!=200){
                        ctx.fail("cant get /secured, statusCode="+r.statusCode());
                    }
                    r.bodyHandler((Buffer b) -> {
                        logger.info("got secured page : "+b.toString());
                        asyncTest.complete();
                    });
                });
                request.exceptionHandler((Throwable ex) -> {
                    ctx.fail(ex);
                });
                request.headers().add("Authorization", "Bearer "+e.result().principal().getString("access_token"));
                request.putHeader("content-type", "application/json");            
                request.end();   
            }
            //asyncTest.complete();
        });
    }
    
    
    @Test
    public void testCompletableFuture(TestContext context){
        logger.info(CompletableFuture.supplyAsync(() -> "Hello").thenApply((String t) -> {
            return t+" world";
        }).join());
    }         
    

    /***
     * a Composition is kind of a plan:
     *      we define "future" events which build up on each other
     *      each step triggers the next one
     *      if any step fails the whole plan is screwed up
     *      at the end we ether succed with an endresult, or we fail
     * 
     * @param ctx 
     */
    @Test
    public void testComposition(TestContext ctx){
        //the Testcontext and "async" object are test-specific objects BUT, as with an httpcontroller in production, we NEED to take 
        // care of it in any circumstances
        final Async asyncTest = ctx.async(); //the async objects asynchronously tells the test framework we done  
        String username = "Hans";
        
        //lets start with our first future event, one day we will have a token response:
        Future<TokenResponse> gotToken = Future.future();
        //We can using "compose" to chain the next step of our "plan" to the gotToken future-object
        gotToken.compose((TokenResponse t) -> {
            //lets plan the next step, let's imagine we need to refresh our token:
            final Future<TokenResponse> gotRefreshedToken = Future.future();
            TokenRequest request = new TokenRequest().setClient_id("client_id")
                                                          .setClient_secret("client_secret")
                                                          .setGrant_type("refresh_token")
                                                          .setRefresh_token(t.getRefresh_token());
            HttpClientRequest postToken = AuthExampleAppTest.client.post("/auth/token");           
            postToken.handler((HttpClientResponse e) -> {
                if(e.statusCode()!=200){
                    gotRefreshedToken.fail("cant refresh token , statuscode = "+e.statusCode()); //we failed :|
                }
                else{
                    e.bodyHandler((Buffer b) -> {
                        try {
                            gotRefreshedToken.complete( WXTools.mapper.readValue(b.toString(), TokenResponse.class) ); //success!!
                        } catch (IOException ex) {
                            gotRefreshedToken.fail(ex);
                        }
                    });
                }
            });
            postToken.exceptionHandler((Throwable e) -> {
                if(!gotRefreshedToken.failed()){
                    gotRefreshedToken.fail(e); //yeah well, we failed :|
                }
            });
            String body = WXTools.toJSON(request);
            postToken.putHeader("content-length", "" + body.length());
            postToken.putHeader("content-type", "application/json");
            postToken.write(body);
            postToken.end();           
            return gotRefreshedToken;    
        }).compose((TokenResponse t) -> { //let's have another plan and chain it after the gotRefreshedToken-future    
            //let's get a page with the tokenresponse from gotRefreshedToken
            Future<String> gotPage = Future.future();            
            HttpClientRequest request = AuthExampleAppTest.client.get("/secured");           
            request.handler((HttpClientResponse e) -> {
                if(e.statusCode()!=200){
                    gotPage.fail("cant get /secured, statusCode="+e.statusCode());
                }
                e.bodyHandler((Buffer b) -> {
                    gotPage.complete(b.toString());
                });
            });
            request.exceptionHandler((Throwable e) -> {
                if(!gotPage.failed()){
                     gotPage.fail(e);
                }
            });
            request.headers().add("Authorization", "Bearer "+t.getAccess_token());
            request.putHeader("content-type", "application/json");            
            request.end();            
            return gotPage;
        }).setHandler((AsyncResult<String> e) -> { //ok baby, our plan ether succeeded or failed
            //When all of these steps are successful, the final future is completed with a success.
            //However, if one of the steps fails, the final future is completed with a failure.            
            if(e.failed()){
                ctx.fail(e.cause());
            }
            ctx.assertEquals(e.result(), "hello "+username);
            asyncTest.complete(); //finish test
        });

        //lets start working on hour dreams, we planned to get a token , right?:D
        TokenRequest tokenRequest = new TokenRequest().setClient_id("client_id")
                                                      .setClient_secret("client_secret")
                                                      .setGrant_type("password");
        tokenRequest.setUsername(username);
                                                     // .setUsername(username);    
        HttpClientRequest postToken = client.post( "/auth/token");
        postToken.handler((HttpClientResponse e) -> {
            if(e.statusCode()!=200){ //not even that is working  man :|
                gotToken.fail("cant create token , statuscode = "+e.statusCode());
            }
            else{
                e.bodyHandler((Buffer b) -> {
                    try {
                        gotToken.complete( WXTools.mapper.readValue(b.toString(), TokenResponse.class) );
                    } catch (IOException ex) {
                        gotToken.fail(ex);
                        return;
                    }
                });
            }
        });
        postToken.exceptionHandler((Throwable e) -> {
           if(!gotToken.failed()){
                gotToken.fail(e);
           }
        });
        String body = WXTools.toJSON(tokenRequest);
        postToken.putHeader("content-length", "" + body.length());
        postToken.putHeader("content-type", "application/json");           
        postToken.write(body);
        postToken.end();       
    }    
    
}
