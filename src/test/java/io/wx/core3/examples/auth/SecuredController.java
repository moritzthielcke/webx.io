/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package io.wx.core3.examples.auth;


import io.wx.core3.examples.auth.beans.User;
import io.wx.core3.http.RequestController;
import io.wx.core3.http.RequestMapping;
import io.wx.core3.http.Resource;
import io.wx.core3.http.traits.TraitConf;
import javax.inject.Inject;

/**
 *
 * @author moritz
 */
@Resource(path = "/secured", traits = {@TraitConf(trait = LoginTrait.class)})
public class SecuredController {
    @Inject RequestController ctr;
    @Inject User u;
    
    @RequestMapping
    public String helloWorld(){
        return "hello "+ u.getUsername();
    }
    
}
