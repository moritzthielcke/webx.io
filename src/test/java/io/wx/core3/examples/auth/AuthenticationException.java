/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package io.wx.core3.examples.auth;

import io.wx.core3.http.exceptions.StatusCode;

/**
 *
 * @author moritz
 */
@StatusCode(value = 401)
public class AuthenticationException extends Exception {

    /**
     * Creates a new instance of <code>ConnectivityException</code> without
     * detail message.
     */
    public AuthenticationException() {
    }

    /**
     * Constructs an instance of <code>ConnectivityException</code> with the
     * specified detail message.
     *
     * @param msg the detail message.
     */
    public AuthenticationException(String msg) {
        super(msg);
    }
    
    public AuthenticationException(String msg, Throwable ex) {
        super(msg,ex);
    }    
    
}
