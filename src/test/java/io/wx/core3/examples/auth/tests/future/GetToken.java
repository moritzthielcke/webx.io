/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package io.wx.core3.examples.auth.tests.future;

import io.vertx.core.Future;
import io.vertx.core.Handler;
import io.vertx.core.http.HttpClientResponse;

/**
 *
 * @author moritz
 */
public class GetToken implements Handler<HttpClientResponse>{
    private final Future future;
    
    public GetToken(Future future){
        this.future = future;
    }

    @Override
    public void handle(HttpClientResponse e) {
        if(e.statusCode() == 201){
            future.complete(e);
        }
        else{
            future.fail("invalid status code =>"+e.statusCode());
        }
    }
    
}
