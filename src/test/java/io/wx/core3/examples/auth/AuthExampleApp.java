/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package io.wx.core3.examples.auth;


import io.vertx.ext.web.templ.ThymeleafTemplateEngine;
import io.wx.core3.config.AppConfig;
import io.wx.core3.config.SimpleConfig;
import io.wx.core3.http.app.AbstractApplication;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 *
 * @author moritz
 */
public class AuthExampleApp extends AbstractApplication{
    private static Logger logger = LogManager.getLogger(AuthExampleApp.class);
    
    
    public static void main(String[] args) throws Exception {
        AppConfig cfg = new SimpleConfig().setPort(1337).setInstanceCount(1)
                                          .setStage(AppConfig.Stage.LOCAL)
                                          .setTemplateEngine(ThymeleafTemplateEngine.create());      
        AuthExampleApp instance = new AuthExampleApp(cfg);
        instance.start().idle();  //@todo implement run() : start + idle?      
    }
    
    
    public AuthExampleApp( AppConfig cfg ) throws Exception{
        super( cfg );        
    }

    @Override
    public String getApplicationPath() {
       return "io.wx.core3.examples.auth";
    }
     
    
}

