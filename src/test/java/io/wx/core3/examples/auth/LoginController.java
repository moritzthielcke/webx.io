/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package io.wx.core3.examples.auth;

import io.vertx.core.MultiMap;
import io.vertx.core.http.CaseInsensitiveHeaders;
import io.wx.core3.examples.auth.beans.User;
import io.wx.core3.examples.auth.beans.TokenResponse;
import io.wx.core3.examples.auth.beans.RefreshToken;
import io.wx.core3.examples.auth.beans.Token;
import io.wx.core3.examples.auth.beans.TokenRequest;
import io.vertx.core.http.HttpMethod;
import io.wx.core3.common.Map2BeanConsumer;
import io.wx.core3.common.WXTools;
import io.wx.core3.http.RequestController;
import io.wx.core3.http.RequestMapping;
import io.wx.core3.http.Resource;
import io.wx.core3.http.app.Application;
import io.wx.core3.http.exceptions.ValidationException;
import io.wx.core3.http.traits.BodyHandlerTrait;
import io.wx.core3.http.traits.TraitConf;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import javax.inject.Inject;
import javax.validation.ConstraintViolation;
import javax.validation.Validator;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 *
 * @author moritz
 */
@Resource(path = "/auth",  traits = {@TraitConf(trait = BodyHandlerTrait.class)})
public class LoginController {
    private static final Logger logger = LogManager.getLogger(LoginController.class);      
    private static final Integer TTL = 3600;
    @Inject RequestController ctr;
    @Inject Application app;
    @Inject Validator bval;
    
    
    /**
     * this map contains valid clientId => client secret combos
     */
    private static final Map<String,String> clientIds = new HashMap<String,String>() {
        {
            put("client_id", "client_secret");
            put("id12345", "secret11345");
        }
    };    
    

    @RequestMapping(method = HttpMethod.POST, path = "/token", consumes = "*/x-www-form-urlencoded", produces = "application/json")
    public TokenResponse getToken() throws Exception{
        logger.info("get token from form");
        MultiMap form2Map = new CaseInsensitiveHeaders();
        for(String entry : ctr.getRequestBodyAsString().split("&")){
            String[] spl = entry.split("=");
            if(spl.length>1){
                form2Map.add(spl[0], URLEncoder.encode(spl[1], "UTF-8"));
            }
        }
        //build request bean from map
        TokenRequest buildRequest = new TokenRequest();
        form2Map.forEach(new Map2BeanConsumer(buildRequest));
        //validate the bean from hand here
        Set<ConstraintViolation<Object>> violations = bval.validate(buildRequest);
        if (violations.size() > 0) {
            throw new ValidationException(violations, "invalid request");
        }
        //pass to implementation
        //return null;
        //return new TokenResponse();   
        return getToken(buildRequest);
    }    
    

    @RequestMapping(method = HttpMethod.POST, path = "/token", consumes =  "application/json")
    public TokenResponse getToken(TokenRequest request) throws Exception{
        logger.info(" get token from json");
        //correct client secret?
        if( !request.getClient_secret().equals(clientIds.get(request.getClient_id()))){
            throw new AuthenticationException("invalid client credentials");
        }
        
        //create token and reponse object:        
        Token token = new Token();
        TokenResponse response = new TokenResponse().setExpires_in(TTL).setRedirect_url("http://test.de"); //the server response

        //set TTL for the token:
        Calendar now =  Calendar.getInstance();
        response.setIssued_at( now.getTimeInMillis() );
        now.add(Calendar.SECOND, TTL);
        token.setValidTo(now.getTimeInMillis());

        
        //handle  grant_type = password
        if("password".equals(request.getGrant_type())){
            //Login user:
            if(request.getUsername()==null || "".equals(request.getUsername())) {
               throw new AuthenticationException("missing username"); //throw validation exception here to make this prettier :)
            }
            User u = new User().setUsername(request.getUsername());
            token.setUser(u);
            
            //generate a refresh token
            RefreshToken refreshToken = new RefreshToken()
                                        .setUser(u)
                                        .setClientId(request.getClient_id())
                                        .setClientSecret(request.getClient_secret());

            //fill response        
            response.setRefresh_token( URLEncoder.encode( app.getEncrypter().encrypt(WXTools.toJSON(refreshToken)) , "UTF-8") );
            response.setAccess_token( URLEncoder.encode( app.getEncrypter().encrypt(WXTools.toJSON(token)) , "UTF-8")) ;
            return response;
        }
         //handle  grant_type = refresh_token
        else if("refresh_token".equals(request.getGrant_type())){
            if(request.getRefresh_token() != null){
                RefreshToken refreshtoken = null;
                try{
                    refreshtoken = WXTools.mapper.readValue(app.getEncrypter().decrypt( URLDecoder.decode(request.getRefresh_token(), "UTF-8") ), 
                                                            RefreshToken.class);
                }catch(Exception ex){
                    logger.error("cant read refresh token",ex);
                    throw new AuthenticationException("invalid refresh token");
                }
                if(!refreshtoken.getClientId().equals(request.getClient_id()) || !refreshtoken.getClientSecret().equals(request.getClient_secret())){
                    logger.error("refresh token clientcredentials missmatch!! (stolen token?)");
                    throw new AuthenticationException("invalid refresh token");
                }
                token.setUser(refreshtoken.getUser());
                response.setRefresh_token( request.getRefresh_token() );
                response.setAccess_token( URLEncoder.encode( app.getEncrypter().encrypt(WXTools.toJSON(token)) , "UTF-8")) ;
                return response;
            }
            throw new AuthenticationException("missing refresh_token"); 
        }
        else{
            throw new AuthenticationException("invalid grant type");
        }
    }
    
    
 
    
    
    
}
