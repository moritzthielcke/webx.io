/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package io.wx.core3.app.controller;

import io.vertx.core.Handler;
import io.vertx.core.buffer.Buffer;
import io.vertx.core.http.HttpClient;
import io.vertx.core.http.HttpClientResponse;
import io.vertx.core.http.HttpMethod;
import io.wx.core3.http.RequestController;
import io.wx.core3.http.RequestMapping;
import io.wx.core3.http.Resource;
import io.wx.core3.http.URLParam;
import javax.inject.Inject;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 *
 * @author moritz
 */
@Resource(path = "/middleware")
public class HelloHttpClient {
    @Inject HttpClient client;
    @Inject RequestController ctr;
    private static Logger logger = LogManager.getLogger(HelloHttpClient.class);   
    
    //@todo dafuq http://localhost:1337/middleware?url=sdjdf causes nullpointer? (the reason seem to be the missing "/" at the start of the url 
    @RequestMapping(method = HttpMethod.GET)
    public void helloHttp(@URLParam(name = "url") String url){
        String myUrl = (url != null) ? url : "/public/index.htm";
        logger.info("???? url???"+myUrl);
        ctr.getResponseBody().append("hi ").append(myUrl).append("<br/>:");
        client.get(1337, "527.0.0.1", myUrl)
        //client.get(url)
        .handler((HttpClientResponse e) -> {
            ctr.getResponseBody().append(" -> ").append(e.statusCode()).append(" ").append(e.statusMessage());
            e.bodyHandler((Buffer e1) -> {
                ctr.getResponseBody().append("<br/>").append(e1);
                client.close();
                ctr.next();
            });
        })
        .exceptionHandler((Throwable e) -> {
            client.close();
            ctr.fail(e);
        })
        .end();
        logger.info("call is made to "+myUrl);
    }
    
    
}
