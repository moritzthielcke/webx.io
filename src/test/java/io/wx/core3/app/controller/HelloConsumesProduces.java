/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package io.wx.core3.app.controller;

import io.wx.core3.http.RequestMapping;
import io.wx.core3.http.RequestController;
import io.wx.core3.http.Resource;
import io.wx.core3.http.traits.TraitConf;
import io.wx.core3.app.traits.MyTrait;
import io.wx.core3.examples.auth.beans.User;
import javax.inject.Inject;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * test routing based on the client accept header
 * 
 * @author Jose Luis Conde Linares-  jlinares@aptly.de /hello-content-type
 */
@Resource(path = "/hello-consumes", consumes = "*/json", produces = {"text/plain", "text/html", "application/json"})
public class HelloConsumesProduces {
    private static Logger logger = LogManager.getLogger(HelloStatefulClass.class);      
    @Inject RequestController ctr;

    
    @RequestMapping(path = "/world1")
    public User world1(){
        User user = new User();
        user.setUsername("jlconde");
        logger.info("HelloProduces.world1() "+ctr.getAcceptableContentType());
        return user;
    }
  
    @RequestMapping(path = "/world2", consumes = "*/xml")
    public User world2(){
        User user = new User();
        user.setUsername("jlconde");
        ctr.getResponse().headers().add("Content-Type", "application/xml");
        logger.info("HelloProduces.world2() "+ctr.getAcceptableContentType());
        return user;
        //return " HelloContentType world2";
    }
 
    @RequestMapping(path = "/world3", traits = {@TraitConf(trait = MyTrait.class)})
    public String world3(){
        logger.info("HelloProduces.world3() "+ctr.getAcceptableContentType());
        return " HelloContentType world3";
    }
  
    @RequestMapping(path = "/world4", produces = {"application/json"})
    public String world4(){
        logger.info("HelloProduces.world2() "+ctr.getAcceptableContentType());
        return " HelloContentType world4 produces->" + ctr.getAcceptableContentType();
    }   
}
