/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package io.wx.core3.app.traits;

import io.vertx.core.Handler;
import io.vertx.ext.web.RoutingContext;
import io.wx.core3.http.app.Application;
import io.wx.core3.http.traits.Trait;

/**
 *
 * @author Jose Luis Conde Linares-  jlinares@aptly.de
 */
public class MyTrait implements Trait {

  @Override
  public Handler<RoutingContext> setupBeforeTrait(Application a){
    return null;
  }
  
  @Override
  public Handler<RoutingContext> setupAfterTrait(Application a) throws Exception{
    return MyHandler.create(a);
  }
}
