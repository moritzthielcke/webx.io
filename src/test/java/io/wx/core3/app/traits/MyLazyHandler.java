/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package io.wx.core3.app.traits;

import io.wx.core3.http.app.Application;
import io.wx.core3.http.handler.Handler;
import io.wx.core3.http.RequestController;
import java.util.ArrayList;
import java.util.List;
import javax.inject.Inject;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 *
 * @author Jose Luis Conde Linares-  jlinares@aptly.de
 */
public class MyLazyHandler extends Handler {
  private static final Logger logger = LogManager.getLogger(MyLazyHandler.class);
  @Inject
  public RequestController ctr;

  private MyLazyHandler(Application a) throws Exception {
    super(a.getCore());
  }
  
  public static MyLazyHandler create(Application a) throws Exception{
    return new MyLazyHandler(a);
  }

  @Override
  public void handle() {
    List<String> data = (List<String>) ctr.getData().get("Trait");
    if (data == null) {
      data = new ArrayList<>();
    }
    logger.info("###    Data->" + data);
    logger.info("###    LazyHandler->" + ctr);
    try {
      Thread.sleep(1900);
    } catch (InterruptedException ex) {
      logger.error("Error " + ex.getMessage());
    }
    ctr.getData().put("Trait", data);
    ctr.next();
  }
}
