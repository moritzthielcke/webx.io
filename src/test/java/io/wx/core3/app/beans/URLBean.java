/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package io.wx.core3.app.beans;

import javax.validation.constraints.NotNull;
import org.apache.bval.constraints.NotEmpty;

/**
 *
 * @author moritz
 */
public class URLBean {
    @NotEmpty(message = "please provide a language")
    @NotNull(message = "please provide a language")
    private String lang;    

    public String getLang() {
        return lang;
    }

    public void setLang(String lang) {
        this.lang = lang;
    }
    
    
    
}
