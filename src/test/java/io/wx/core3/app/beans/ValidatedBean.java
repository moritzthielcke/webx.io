/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package io.wx.core3.app.beans;

import javax.validation.constraints.NotNull;
import org.apache.bval.constraints.NotEmpty;

/**
 *
 * @author moritz
 */
public class ValidatedBean {
    @NotEmpty(message = "please provide a username")
    @NotNull(message = "please provide a username")
    private String username;
    @NotNull(message = "please provide a username")    
    @NotEmpty(message = "please provide a password")
    private String password;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
    
    
    
    
    
}
