# webx #

a webframework based on embedded vertx



### How do I start ###

webx.io / src / test / java / io / wx / core3 / MyApplication.java 


### Example Projects ###

https://bitbucket.org/moritzthielcke/webx.io-3.0-example-todoserver

### Known Issues ###

This projects required you to have JCE unlimited strength jurisdiction policy files installed.
For more info read  http://stackoverflow.com/questions/6481627/java-security-illegal-key-size-or-default-parameters

### Internal documentation ###

https://codemitte.atlassian.net/wiki/display/AI/Webx3

